# Pulse Android Demo

This is a simple working demo app that utilizes our Android SDK. The complete Android documentation can be found [here](http://support.pulseid.com/sdk/android/index.html).

### Quickstart 

Once you've cloned this repo, open it with Android Studio. After that you need to setup your platform credentials in `PulseSdkApplication.java` by changing the following placeholders:

```
private static final String APP_KEY = "YOUR_APP_KEY"; // TODO: Replace with your own!
private static final String APP_URL = "YOUR_APP_URL"; // TODO: Replace with your own!
```

In case you encounter any issues please email: support@pulseid.com