package com.pulse.demo;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class DeepLinkActivity extends AppCompatActivity {

    TextView tvDeeplink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deeplink);
        tvDeeplink = findViewById(R.id.tv_deeplink);
        Intent intent = getIntent();
        Uri data = intent.getData();
        if (data != null) {
            tvDeeplink.setText(data.getHost());
        }
    }
}
