package com.pulse.demo;

import android.app.Application;

import com.pulseid.sdk.PulseSdk;
import com.pulseid.sdk.PulseSdkConfig;

public class PulseSdkApplication extends Application {
    public static final String TAG = "PulseSdkApplication";

    private static final String APP_KEY = "YOUR_APP_KEY"; // TODO: Replace with your own!
    private static final String APP_URL = "YOUR_APP_URL"; // TODO: Replace with your own!

    private static PulseSdkApplication application;
    private PulseSdk pulseSdk;

    @Override
    public void onCreate() {
        super.onCreate();

        application = this;

        PulseSdkConfig config = new PulseSdkConfig(APP_KEY, APP_URL);
        pulseSdk = new PulseSdk(this, config);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public static PulseSdkApplication getInstance() {
        return application;
    }

    public PulseSdk getPulseSdk() {
        return pulseSdk;
    }
}