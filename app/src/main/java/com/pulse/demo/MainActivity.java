package com.pulse.demo;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.pulseid.sdk.PulseEvents;
import com.pulseid.sdk.PulseSdk;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    public static final int REQUEST_PERMISSIONS_REQUEST_CODE = 42;

    private PulseSdk pulseSdk;

    private BroadcastReceiver startupReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String intentAction = intent.getAction();

            if (PulseEvents.SDK_STARTED.equals(intentAction)) {
                Log.i(TAG, "SDK Started!");
            } else {
                Log.i(TAG, "SDK Stopped!");
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(com.pulse.demo.R.layout.activity_demo);

        TextView mTextView = findViewById(R.id.sdkVersionName);
        mTextView.setText(BuildConfig.VERSION_NAME);

        pulseSdk = PulseSdkApplication.getInstance().getPulseSdk();

        IntentFilter startupIntentFilter = new IntentFilter();
        startupIntentFilter.addAction(PulseEvents.SDK_STARTED);
        startupIntentFilter.addAction(PulseEvents.SDK_STOPPED);
        LocalBroadcastManager.getInstance(this).registerReceiver(startupReceiver,
                startupIntentFilter);

        if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //Step #1: Request user permissions
            requestPermissions();
        }
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                REQUEST_PERMISSIONS_REQUEST_CODE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.i(TAG, "Permission granted.");
                //Step #2: Start the SDK
                pulseSdk.start();
            } else {
                Log.i(TAG, "Permission denied");
            }
        }
    }
}